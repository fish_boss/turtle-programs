--Pastebin link: zEkiPsYV
--Program for mining a spiral strip mine
local clearEC = 16
local fuelEC = 15
local maxCap = 75
--function for checking/emptying inventory
function checkInv()
	local full = true
	for i=1,16 do
		if i~=clearEC and i~=fuelEC then
			if turtle.getItemCount(i) == 0 then
				full = false
			end
		end
	end
	if full then
		local moved = false
		if turtle.detectDown() then
			turtle.up()
			moved = true
		end
		turtle.select(clearEC)
		turtle.placeDown()
		for i=1,16 do
			if i~=clearEC and i~=fuelEC then
				turtle.select(i)
				turtle.dropDown()
			end
		end
		turtle.select(clearEC)
		turtle.digDown()
		if moved then
			turtle.down()
		end
	end
end

--function for refueling 
function fuelUp()
	local moved = false
	if turtle.detectDown() then
		turtle.up()
		moved = true
	end
	turtle.select(fuelEC)
	turtle.placeDown()
	turtle.suckDown()
	turtle.refuel()
	turtle.select(fuelEC)
	turtle.digDown()
	if moved then
		turtle.down()
	end
end

--function for moving forward
function proceed(cap)
	for i=0,cap do
		if turtle.getFuelLevel() < 5 then
			fuelUp()
		end
		while not turtle.forward() do
			if turtle.detect() then
				if turtle.dig() then
					checkInv()
				end
			elseif turtle.attack() then
				checkInv()
			else
				sleep(0.5)
			end
		end
		if turtle.detectUp() then
			while turtle.detectUp() do
				if turtle.digUp() then
					checkInv()
				end
				sleep(0.5)
			end
		end
	end
end

--main program
print("What is the y-coord standing on the turtle?")
local height = read()
local cap = 2
for i=0,height-12 do
	if turtle.detectDown() then
		if turtle.digDown() then
			checkInv()
		end
	elseif turtle.attackDown() then
		checkInv()
	end
	if turtle.getFuelLevel() < 5 then
		fuelUp()
	end
	turtle.down()
end
while cap<maxCap do
	proceed(cap)
	turtle.turnLeft()
	cap=cap+1
	proceed(cap)
	turtle.turnLeft()
	cap=cap+2
end
--return home