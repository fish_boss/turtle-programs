--Pastebin link: ShH8CHVM
--Program for refueling turtle in a pool of lava
local bucketSlot = 1

function fuelForward()
	turtle.select(bucketSlot)
	turtle.place()
	turtle.refuel()
	if not turtle.forward() then
		return false
	end
	return true
end
for i=0,100 do
	while fuelForward() do end
	turtle.turnRight()
	while not fuelForward() do
		turtle.turnRight()
		turtle.forward()
		turtle.turnLeft()
	end
	turtle.turnRight()
	while fuelForward() do end
	turtle.turnLeft()
	while not fuelForward() do
		turtle.turnLeft()
		turtle.forward()
		turtle.turnRight()
	end
	turtle.turnLeft()
end
turtle.up()
turtle.forward()
turtle.forward()