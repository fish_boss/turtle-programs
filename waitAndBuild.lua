--Pastebin link: A09ZwpR9
--Program for building a straight path with other turtles
function wait(side)
	while true do
		os.pullEvent("redstone")
		if rs.getInput(side) then
			break
		end
	end
end

function findItems()
	for i=1,16 do
		if turtle.getItemCount() ~= 0 then
			return i
		end
	end
	print("All out of items :^(")
end

function proceed()
	local itemSlot
	if turtle.forward() then
		if not turtle.place() then
			itemSlot = findItems()
			turtle.select(itemSlot)
			turtle.place()
		end
	else
		print("Error: Could not move forward.")
	end
end

print("Starting obsidian mining program...")
while true do
	wait("left")
	for i=0,200 do
		proceed()
	end
end